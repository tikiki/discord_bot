module Bot
  module DiscordCommands
    module AdminCommands
      extend Discordrb::Commands::CommandContainer

      command([:clean, :prune], help_available: false) do
      # @type event [Discordrb::Commands::CommandEvent]
      # @type [String] num
      |event, num|
        # We prune n+1 messages because we also remove the command
        event.channel.prune(num.to_i + 1) if event.user.id == 101005517345800192
      end

    end
  end
end
