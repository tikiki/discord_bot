require 'time_diff' # For "uptime" command

module Bot
  module DiscordCommands
    module BasicCommands
      extend Discordrb::Commands::CommandContainer

      PING_DESCRIPTION = 'Répond "Pong". Utile pour vérifier que le bot est toujours vivant.'
      UPTIME_DESCRIPTION = "Donne l'uptime du bot."
      VERSION_DESCRIPTION = 'Donne la version du bot.'
      ONLINE_DESCRIPTION = 'Compte le nombre de personnes connectées actuellement (bot exclus).'

      COOLDOWN_MESSAGE = 'Calme toi !'

      bucket :ping, limit: 1, time_span: 30
      bucket :uptime, limit: 1, time_span: 30
      bucket :version, limit: 1, time_span: 30

      command(:ping, bucket: :ping, description: PING_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      |event|
        # @type m [Discordrb::Message]
        m = event.respond "Pong #{event.user.mention} !"
        m.edit "Pong #{event.user.mention} ! (#{Time.now - event.timestamp}s)"
        sleep 5
        event.message.delete
        m.delete
      end

      command(:uptime, bucket: :uptime, description: UPTIME_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      |event|
        uptime = Time.diff(Time.now, START_TIME)
        "Uptime : `#{uptime[:day]}j #{uptime[:hour]}h #{uptime[:minute]}min`"
      end

      command(:version, bucket: :version, description: VERSION_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      |event|
        "Version : `#{configatron.version}`"
      end

      command(:online, description: ONLINE_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      |event|
        all_count = event.server.online_members(include_idle: true, include_bots: true).count
        members_count = event.server.online_members(include_idle: true, include_bots: false).count
        active_count = event.server.online_members(include_idle: false, include_bots: false).count
        idle_count = members_count - active_count
        bot_count = all_count - members_count
        active_string = "#{active_count} personne#{'s' if active_count >= 2}"
        idle_string = "#{idle_count} inactif#{'s' if idle_count >= 2}"
        bot_string = "#{bot_count} bot#{'s' if bot_count >= 2}"
        event << "#{ active_string } en ligne (+ #{ idle_string }, #{ bot_string })"
      end

    end
  end
end
