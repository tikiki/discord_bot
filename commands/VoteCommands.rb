module Bot
  module DiscordCommands
    module VoteCommands
      extend Discordrb::Commands::CommandContainer

      # ID of the #suggestions channel
      SUGGESTIONS_CHANNEL_ID = 243440023234543617
      VOTE_DESCRIPTION = 'Démarre un vote. :warning: Ne fonctionne que sur <#243440023234543617>.'

      command(:vote, description: VOTE_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      | event, *message |
        reactions = %w(⬆ ⬇)

        if event.channel == SUGGESTIONS_CHANNEL_ID || $cr_env.dev?
          # @type vote_message [Discordrb::Message]
          vote_message = event.message
          # @type channel [Discordrb::Channel]
          channel = event.channel
          # @type author [Discordrb::User]
          author = event.user

          if message.nil? || message.empty?
            channel.send_temporary_message "Ta proposition est vide #{author.mention} ! Recommence !", 4
            vote_message.delete
            next
          end
          vote_message.pin
          history = event.channel.history 10
          history.find{ |m| m.user.id == configatron.client_id }&.delete

          reactions.each do |r|
            vote_message.react r
          end

          return nil

        else
          event.message.delete
          event.channel.send_temporary_message "#{event.user.mention} Cette commande ne fonctionne que dans <#243440023234543617> !", TEMP_MESSAGE_DELAY
        end
      end

    end
  end
end
