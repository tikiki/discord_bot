#############
# BOT SETUP #
#############

require 'bundler/setup'
require 'dotenv'
require 'configatron'
require 'discordrb'
require './src/crbot_environment'

Dotenv.load
# @type $cr_env [CrbotEnvironment]
$cr_env = CrbotEnvironment.instance
$cr_env.environment = ENV['CRBOT_ENVIRONMENT']

case
  when $cr_env.dev?
    puts 'Running in development mode'
  when $cr_env.prod?
    puts 'Running in production mode'
  else
    warn "Unrecognized 'CRBOT_ENVIRONMENT' defined!"
    warn 'Shutting down...'
    exit
end

if File.file? 'config.rb'
  require_relative 'config.rb'
else
  warn 'No config file! Get a fresh copy of the bot using git and set values accordingly.'
  warn 'Shutting down...'
  exit
end

module Bot
  # @type $cr_bot [Discordrb::Commands::CommandBot]
  $cr_bot = Discordrb::Commands::CommandBot.new token: configatron.token,
                                                client_id: configatron.client_id,
                                                prefix: configatron.prefix

  puts "This bot's invite URL is #{$cr_bot.invite_url}."
  puts 'Click on it to invite it to your server.'

  ###################
  # LOADING MODULES #
  ###################

  module DiscordCommands; end

  Dir['commands/*.rb'].each { |mod| load mod }
  DiscordCommands.constants.each do |mod|
    $cr_bot.include! DiscordCommands.const_get mod
  end

  #############
  # CONSTANTS #
  #############

  # Delay before deleting self-destroying message
  TEMP_MESSAGE_DELAY = 4
  # Delay before deleting self-destroying message (shorter)
  SHORT_TEMP_MESSAGE_DELAY = 2
  # ID of the admin role
  ADMIN_PERM_ROLE_ID = 231050777240535040

  ################
  # WE ARE READY #
  ################

  $cr_bot.ready do
    START_TIME = Time.now
    $cr_bot.game = "Version #{configatron.version}"
  end

  $cr_bot.run
end
